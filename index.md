---
layout: home
title: Lens by HTML5 UP
---

<!-- Thumbnail -->
<section id="thumbnails">{% for photo in site.photos %}
	<article>
		<a class="thumbnail" href="{{ photo.image }}" data-position="left center"><img src="{{ photo.thumbnail }}" alt="" /></a>
		<h2>{{ photo.title }}</h2>
		<p>{{ photo.caption }}</p>
		{% for category in photo.categories %}
		    <h2><span class="badge badge-primary">{{ category }}</span></h2>
		{% endfor %}
	</article>
{% endfor %}</section>
